<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table = 'buyers';

    public function diary_takens()
    {
    	return $this->hasMany('App\DiaryTaken');
    }
    public function eraser_takens()
    {
    	return $this->hasMany('App\EraserTaken');
    }

    public function pen_takens()
    {
    	return $this->hasMany('App\PenTaken');
    }
}
