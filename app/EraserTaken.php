<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EraserTaken extends Model
{
    //
	protected $table = 'eraser_takens';
    public function buyer()
    {
    	return $this->belongsTo('App\Buyer');
    }
}
