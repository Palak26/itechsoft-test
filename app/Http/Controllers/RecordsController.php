<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Str;
use DB;

class RecordsController extends Controller
{
    //
    public function store(Request $request)
    {
    	$json = file_get_contents(storage_path('app/public/records.json'));
		$json_array = json_decode($json,true);

		foreach (array_chunk($json_array['RECORDS'],1000) as $t)
		{
		    DB::table('records')->insert($t);

		}

		return redirect('/')->with('success', 'successfully json data stored in database');
    }
}
