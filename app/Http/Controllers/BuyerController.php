<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use App\Buyer;

class BuyerController extends Controller
{
    //
    public function SecondBuyerEloquent()
    {
    	$data_diary = [];
    	$data_eraser = [];
    	$data_pen = [];
    	$finalArr = [];

    	$total_diary = Buyer::select('buyers.id','buyers.name',\DB::raw('sum(diary_takens.amount) as total'))
    				->leftjoin('diary_takens','diary_takens.buyer_id','=','buyers.id')
    				->groupBy('diary_takens.buyer_id','buyers.id','buyers.name')
    				->orderBy('buyers.id')
    				->get();

    	$total_eraser = Buyer::select('buyers.id',\DB::raw('sum(eraser_takens.amount) as total'))
    							->leftjoin('eraser_takens','eraser_takens.buyer_id','=','buyers.id')
    							->groupBy('eraser_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$total_pen = Buyer::select('buyers.id',\DB::raw('sum(pen_takens.amount) as total'))
    							->leftjoin('pen_takens','pen_takens.buyer_id','=','buyers.id')
    							->groupBy('pen_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$i=0;
    	foreach ($total_diary as $key => $value) {
    		$data_diary['id'][$i] = $value->id;
    		$data_diary['name'][$i] = $value->name;
    		$data_diary['total_diary'][$i] = $value->total ?? 0;

    		$i++;
    	}

    	$j=0;
    	foreach ($total_eraser as $key => $value) {
    		$data_eraser['total_eraser'][$j] = $value->total ?? 0;
    		$j++;
    	}

    	$k=0;
    	foreach ($total_pen as $key => $value) {
    		$data_pen['total_pen'][$k] = $value->total ?? 0;
    		$k++;
    	}

    	 $mergeArr = array_merge($data_diary,$data_eraser,$data_pen);
    	 for($i=0;$i<count($mergeArr['id']);$i++) {
    	 	# code...
    	 	$finalArr[$i] = [

    	 				'id'			=> $mergeArr['id'][$i],
    	 				'name'			=> $mergeArr['name'][$i],
    	 				'total_diary'	=> $mergeArr['total_diary'][$i],
    	 				'total_eraser'	=> $mergeArr['total_eraser'][$i],
    	 				'total_pen'		=> $mergeArr['total_pen'][$i],
    	 				'total_items'	=> $mergeArr['total_diary'][$i] + $mergeArr['total_eraser'][$i] + $mergeArr['total_pen'][$i]
    	 			];
   	 }

		$sortArray = [];
		foreach($finalArr as $person){
		    foreach($person as $key=>$value){
		        if(!isset($sortArray[$key])){
		            $sortArray[$key] = [];
		        }
		        $sortArray[$key][] = $value;
		    }
		}

		$orderby = "total_items";
		array_multisort($sortArray[$orderby],SORT_DESC,$finalArr);


		return view('task1',['dataSecBuyer'=>$finalArr[1],'task'=>'1a']);
    }

    public function SecondBuyerWithoutEloquent()
    {
    	$data_diary = [];
    	$data_eraser = [];
    	$data_pen = [];
    	$finalArr = [];

    	$total_diary = DB::table('buyers')->select('buyers.id','buyers.name',\DB::raw('sum(diary_takens.amount) as total'))
    				->leftjoin('diary_takens','diary_takens.buyer_id','=','buyers.id')
    				->groupBy('diary_takens.buyer_id','buyers.id','buyers.name')
    				->orderBy('buyers.id')
    				->get();

    	$total_eraser = DB::table('buyers')->select('buyers.id',\DB::raw('sum(eraser_takens.amount) as total'))
    							->leftjoin('eraser_takens','eraser_takens.buyer_id','=','buyers.id')
    							->groupBy('eraser_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$total_pen = DB::table('buyers')->select('buyers.id',\DB::raw('sum(pen_takens.amount) as total'))
    							->leftjoin('pen_takens','pen_takens.buyer_id','=','buyers.id')
    							->groupBy('pen_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$i=0;
    	foreach ($total_diary as $key => $value) {
    		$data_diary['id'][$i] = $value->id;
    		$data_diary['name'][$i] = $value->name;
    		$data_diary['total_diary'][$i] = $value->total ?? 0;

    		$i++;
    	}

    	$j=0;
    	foreach ($total_eraser as $key => $value) {
    		$data_eraser['total_eraser'][$j] = $value->total ?? 0;
    		$j++;
    	}

    	$k=0;
    	foreach ($total_pen as $key => $value) {
    		$data_pen['total_pen'][$k] = $value->total ?? 0;
    		$k++;
    	}

    	 $mergeArr = array_merge($data_diary,$data_eraser,$data_pen);
    	 for($i=0;$i<count($mergeArr['id']);$i++) {
    	 	# code...
    	 	$finalArr[$i] = [

    	 				'id'			=> $mergeArr['id'][$i],
    	 				'name'			=> $mergeArr['name'][$i],
    	 				'total_diary'	=> $mergeArr['total_diary'][$i],
    	 				'total_eraser'	=> $mergeArr['total_eraser'][$i],
    	 				'total_pen'		=> $mergeArr['total_pen'][$i],
    	 				'total_items'	=> $mergeArr['total_diary'][$i] + $mergeArr['total_eraser'][$i] + $mergeArr['total_pen'][$i]
    	 			];
   	 }

		$sortArray = [];
		foreach($finalArr as $person){
		    foreach($person as $key=>$value){
		        if(!isset($sortArray[$key])){
		            $sortArray[$key] = [];
		        }
		        $sortArray[$key][] = $value;
		    }
		}

		$orderby = "total_items";
		array_multisort($sortArray[$orderby],SORT_DESC,$finalArr);


		return view('task1',['dataSecBuyerWithoutEloq'=>$finalArr[1],'task'=>'1a']);
    }



    public function WithEloquent()
    {
    	$data_diary = [];
    	$data_eraser = [];
    	$data_pen = [];
    	$finalArr = [];

    	$total_diary = Buyer::select('buyers.id','buyers.name',\DB::raw('sum(diary_takens.amount) as total'))
    				->leftjoin('diary_takens','diary_takens.buyer_id','=','buyers.id')
    				->groupBy('diary_takens.buyer_id','buyers.id','buyers.name')
    				->orderBy('buyers.id')
    				->get();

    	$total_eraser = Buyer::select('buyers.id',\DB::raw('sum(eraser_takens.amount) as total'))
    							->leftjoin('eraser_takens','eraser_takens.buyer_id','=','buyers.id')
    							->groupBy('eraser_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$total_pen = Buyer::select('buyers.id',\DB::raw('sum(pen_takens.amount) as total'))
    							->leftjoin('pen_takens','pen_takens.buyer_id','=','buyers.id')
    							->groupBy('pen_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$i=0;
    	foreach ($total_diary as $key => $value) {
    		$data_diary['id'][$i] = $value->id;
    		$data_diary['name'][$i] = $value->name;
    		$data_diary['total_diary'][$i] = $value->total ?? 0;

    		$i++;
    	}

    	$j=0;
    	foreach ($total_eraser as $key => $value) {
    		$data_eraser['total_eraser'][$j] = $value->total ?? 0;
    		$j++;
    	}

    	$k=0;
    	foreach ($total_pen as $key => $value) {
    		$data_pen['total_pen'][$k] = $value->total ?? 0;
    		$k++;
    	}

    	 $mergeArr = array_merge($data_diary,$data_eraser,$data_pen);
    	 for($i=0;$i<count($mergeArr['id']);$i++) {
    	 	# code...
    	 	$finalArr[$i] = [

    	 				'id'			=> $mergeArr['id'][$i],
    	 				'name'			=> $mergeArr['name'][$i],
    	 				'total_diary'	=> $mergeArr['total_diary'][$i],
    	 				'total_eraser'	=> $mergeArr['total_eraser'][$i],
    	 				'total_pen'		=> $mergeArr['total_pen'][$i],
    	 				'total_items'	=> $mergeArr['total_diary'][$i] + $mergeArr['total_eraser'][$i] + $mergeArr['total_pen'][$i]
    	 			];
   	 }

		$sortArray = [];
		foreach($finalArr as $person){
		    foreach($person as $key=>$value){
		        if(!isset($sortArray[$key])){
		            $sortArray[$key] = [];
		        }
		        $sortArray[$key][] = $value;
		    }
		}

		$orderby = "total_items";
		array_multisort($sortArray[$orderby],SORT_ASC,$finalArr);

		return view('task1',['data'=>$finalArr,'task'=>'1b']);
    }

    public function WithoutEloquent()
    {
    	$data_diary = [];
    	$data_eraser = [];
    	$data_pen = [];
    	$finalArr = [];

    	$total_diary = DB::table('buyers')->select('buyers.id','buyers.name',\DB::raw('sum(diary_takens.amount) as total'))
    				->leftjoin('diary_takens','diary_takens.buyer_id','=','buyers.id')
    				->groupBy('diary_takens.buyer_id','buyers.id','buyers.name')
    				->orderBy('buyers.id')
    				->get();

    	$total_eraser = DB::table('buyers')->select('buyers.id',\DB::raw('sum(eraser_takens.amount) as total'))
    							->leftjoin('eraser_takens','eraser_takens.buyer_id','=','buyers.id')
    							->groupBy('eraser_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$total_pen = DB::table('buyers')->select('buyers.id',\DB::raw('sum(pen_takens.amount) as total'))
    							->leftjoin('pen_takens','pen_takens.buyer_id','=','buyers.id')
    							->groupBy('pen_takens.buyer_id','buyers.id')
    							->orderBy('buyers.id')
    							->get();

    	$i=0;
    	foreach ($total_diary as $key => $value) {
    		$data_diary['id'][$i] = $value->id;
    		$data_diary['name'][$i] = $value->name;
    		$data_diary['total_diary'][$i] = $value->total ?? 0;

    		$i++;
    	}

    	$j=0;
    	foreach ($total_eraser as $key => $value) {
    		$data_eraser['total_eraser'][$j] = $value->total ?? 0;
    		$j++;
    	}

    	$k=0;
    	foreach ($total_pen as $key => $value) {
    		$data_pen['total_pen'][$k] = $value->total ?? 0;
    		$k++;
    	}

    	 $mergeArr = array_merge($data_diary,$data_eraser,$data_pen);
    	 for($i=0;$i<count($mergeArr['id']);$i++) {
    	 	# code...
    	 	$finalArr[$i] = [

    	 				'id'			=> $mergeArr['id'][$i],
    	 				'name'			=> $mergeArr['name'][$i],
    	 				'total_diary'	=> $mergeArr['total_diary'][$i],
    	 				'total_eraser'	=> $mergeArr['total_eraser'][$i],
    	 				'total_pen'		=> $mergeArr['total_pen'][$i],
    	 				'total_items'	=> $mergeArr['total_diary'][$i] + $mergeArr['total_eraser'][$i] + $mergeArr['total_pen'][$i]
    	 			];
   	 }

		$sortArray = [];
		foreach($finalArr as $person){
		    foreach($person as $key=>$value){
		        if(!isset($sortArray[$key])){
		            $sortArray[$key] = [];
		        }
		        $sortArray[$key][] = $value;
		    }
		}

		$orderby = "total_items";
		array_multisort($sortArray[$orderby],SORT_ASC,$finalArr);

		return view('task1',['datawithouteloq'=>$finalArr,'task'=>'1b']);
    }




}
