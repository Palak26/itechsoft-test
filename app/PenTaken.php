<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenTaken extends Model
{
    //

    protected $table = 'pen_takens';
    public function buyer()
    {
    	return $this->belongsTo('App\Buyer');
    }
}
