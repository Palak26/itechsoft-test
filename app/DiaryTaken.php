<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiaryTaken extends Model
{
    //
    protected $table = 'diary_takens';

    public function buyer()
    {
    	return $this->belongsTo('App\Buyer');
    }
}
