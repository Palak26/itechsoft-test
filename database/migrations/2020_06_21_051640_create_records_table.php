<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_statement')->default(1);
            $table->string('financial_instrument_code')->nullable();
            $table->enum('action', array('buy', 'sell'))->default('buy');
            $table->decimal('entry_price', 19, 8)->nullable();
            $table->decimal('closed_price', 19, 8)->nullable();
            $table->decimal('take_profit_1', 19, 8)->nullable();
            $table->decimal('stop_loss_1', 19, 8)->nullable();
            $table->integer('signal_result')->nullable();
            $table->integer('status')->default(0);
            $table->string('statement_batch')->nullable();
            $table->timestamp('closed_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
