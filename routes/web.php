<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('task', function () {
    return view('tasks');
});


/****** TASK - 1 ********/
Route::get('second-buyer-eloquent','BuyerController@SecondBuyerEloquent');
Route::get('second-buyer-no-eloquent','BuyerController@SecondBuyerWithoutEloquent');
Route::get('purchase-list-eloquent','BuyerController@WithEloquent');
Route::get('purchase-list-no-eloquent','BuyerController@WithoutEloquent');


/****** TASK - 1 ********/


/****** TASK - 2 ********/
Route::get('record-transfer','RecordsController@store');
/****** TASK - 2 ********/

/****** TASK - 3 ********/
Route::get('define-callback-js', function () {
    return view('tasks');
});
/****** TASK - 3 ********/

/****** TASK - 4 ********/
Route::get('sort-js', function () {
    return view('sort');
});

Route::get('filter-js', function () {
    return view('sort');
});

Route::get('map-js', function () {
    return view('sort');
});

Route::get('reduce-js', function () {
    return view('sort');
});

/****** TASK - 4 ********/
