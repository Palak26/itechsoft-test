<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Itech Test</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;

            font-weight: 600;
            height: 100vh;
            margin: 0;
            padding: 10px;
        }
        table{
            margin-left: 45px;
        }

        td{
            border: 1px solid #eee;
            padding: 3px;
            text-align: center;
        }
        code{
            color: #007b00;
        }
        a{
            color: red;
        }
    </style>
</head>
<body>
    <div class="content">

        <a href="{{url('/')}}" class="btn btn-sm btn-success">Go to home</a>

        <h3><center>TASKS</center></h3>

        <h3><center>NAME : IMTIAZ AHAMED</center></h3>


        <h4 style="color: green;font-weight: bold;">TASK-01</h4>

        <input type="number" class="form-control" id="ageid">
         <span id="msg" style="color: green" ></span>
        <br>
        <button type="button" class="btn btn-sm btn-info" onclick="checkAgeByButtonClick()">Check Age</button>





        @if(\Session::get('task') == 2)

            <h4 style="color: green;font-weight: bold;">TASK-02</h4>

            <br/>
                <a href="{{url('record-transfer')}}" class="btn btn-sm btn-primary">Click here to record json data into database</a>

            @if(\Session::has('success'))
                <div class="alert alert-success">
                  <strong>Success!</strong> {!! \Session::get('success') !!}
                </div>
            @endif


       @elseif(\Session::get('task') ==3)


        <hr/>

       <h4 style="color: green;font-weight: bold;">TASK-03</h4>

        <input type="number" class="form-control" id="ageid">
         <span id="msg" style="color: green" ></span>
        <br>
        <button type="button" class="btn btn-sm btn-info" onclick="checkAgeByButtonClick()">Check Age</button>

        @endif




    </div>


<script>

    /****** TASK - 03 ************/
    var data = {email:'trump@gmail.com', age:70}; // input json.

    checkAge(data, function(email){
        console.log('Email is valid');
    });

    function checkAge(data) {
       if (data.age < 18) {
        console.log('not valid');
        } else {
        console.log('valid age');
        }
    }
    /****** TASK - 03 ************/



    function checkAgeByButtonClick() {

        age = document.getElementById('ageid').value;

       if (age < 18) {
             document.getElementById("msg").innerText = 'Age is less than 18';
        }else {
         document.getElementById("msg").innerText = 'Age is greater than 18';
        }
    }


</script>




</body>
</html>
