<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Itech Test</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;

            font-weight: 600;
            height: 100vh;
            margin: 0;
            padding: 10px;
        }
        table{
            margin-left: 45px;
        }

        td{
            border: 1px solid #eee;
            padding: 3px;
            text-align: center;
        }
        code{
            color: #007b00;
        }
        a{
            color: red;
        }
    </style>
</head>
<body>
    <div class="content">

        <a href="{{url('/')}}" class="btn btn-sm btn-success">Go to home</a>

        <h3><center>TASKS</center></h3>

        <h3><center>NAME : IMTIAZ AHAMED</center></h3>

        <h4>Task-04</h4>


        <h4 style="color: green;font-weight: bold;">SORT</h4>

	        <button onclick="sortFunction()">Click here to sort the array</button>
			<p id="arrayDiv"></p>

			<br/>

			<p>Foreach data:</p>
			<p id="arrayForeach"></p>


		<h4 style="color: green;font-weight: bold;">FILTER</h4>

			Salaries: <p id="filterDiv"></p>

			<button onclick="filterFunction()">Click here to filter data > 5000</button>

		<h4 style="color: green;font-weight: bold;">ARRAY MAP</h4>

			numbers: <p id="mapDiv"></p>

			<button onclick="MapFunction()">Click here to map data</button>

		<h4 style="color: green;font-weight: bold;">MAP REDUCE</h4>

			numbers: <p id="mapredDiv"></p>

			<button onclick="MapRedFunction()">Click here to map reduce data</button>



			<br/>
    </div>


<script>

	/************** SORT ****************/

	var names = ["Karim", "Abdul", "Rifat", "Javed","Noyon","Babul"];
		document.getElementById("arrayDiv").innerHTML = names;

	names.forEach(foreachFunction);

	function sortFunction() {
		names.sort();
		document.getElementById("arrayDiv").innerHTML = names;
	}

	function foreachFunction(item) {
	  	document.getElementById("arrayForeach").innerHTML += item + "<br>";
	}

	/************** SORT ****************/

	/************** FILTER ****************/

	var salaries = [5000,10000,12000,15000,25000];
	document.getElementById("filterDiv").innerHTML = salaries;

	function filterSal(sal) {
	  return sal > 5000;
	}


	function filterFunction() {
		  document.getElementById("filterDiv").innerHTML = salaries.filter(filterSal);
		}


	/************** FILTER ****************/

	/************** MAP ****************/

	var numbers = [4, 9, 16, 25];
		document.getElementById("mapDiv").innerHTML = numbers;

	function MapFunction() {
		var mapnum = numbers.map(Math.sqrt)
		document.getElementById("mapDiv").innerHTML = mapnum;
	}
	/************** MAP ****************/

	/************** REDUCE ****************/
	var numbers = [15, 12, 20, 25];

	document.getElementById("mapredDiv").innerHTML = numbers;

	function getSumOfTotal(total, num) {
		  return total + Math.round(num);
	}
	function MapRedFunction(item) {
		  document.getElementById("mapredDiv").innerHTML = numbers.reduce(getSumOfTotal, 0);
	}

	/************** REDUCE ****************/



</script>


</body>
</html>
