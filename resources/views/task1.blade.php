<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Itech Test</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;

            font-weight: 600;
            height: 100vh;
            margin: 0;
            padding: 10px;
        }
        table{
            margin-left: 45px;
        }

        td{
            border: 1px solid #eee;
            padding: 3px;
            text-align: center;
        }
        code{
            color: #007b00;
        }
        a{
            color: red;
        }
    </style>
</head>
<body>
    <div class="content">

    	<a href="{{url('/')}}" class="btn btn-sm btn-success">Go to home</a>

    	<a href="{{url('second-buyer-eloquent')}}" class="btn btn-sm btn-primary">second-buyer-eloquent</a>
        <a href="{{url('second-buyer-no-eloquent')}}" class="btn btn-sm btn-primary">second-buyer-no-eloquent</a>
        <a href="{{url('purchase-list-eloquent')}}" class="btn btn-sm btn-primary">purchase-list-eloquent</a>
        <a href="{{url('purchase-list-no-eloquent')}}" class="btn btn-sm btn-primary">purchase-list-no-eloquent</a>

    	@if($task =='1a')

    	@if(isset($dataSecBuyer))

	    	<h4>Second Highest - With Eloquent</h4>

	    		<table class="table table-bordered">

	    			<thead>
	    				<tr>
	    				 	<th>Buyer id</th>
	    				 	<th>Buyer Name</th>
	    				 	<th>Total Diary Taken</th>
	    				 	<th>Total Pen Taken</th>
	    				 	<th>Total Eraser Taken</th>
	    				 	<th>Total items Taken</th>
	    				</tr>
	    			</thead>
	    			<tbody>


	    				<tr>
	    				 	<td>{{$dataSecBuyer['id']}}</td>
			    			<td>{{$dataSecBuyer['name']}}</td>
			    			<td>{{$dataSecBuyer['total_diary']}}</td>
			    			<td>{{$dataSecBuyer['total_pen']}}</td>
			    			<td>{{$dataSecBuyer['total_eraser']}}</td>
			    			<td>{{$dataSecBuyer['total_items']}}</td>
	    				</tr>

	    			</tbody>

	    		</table>
	    @endif

    		@if(isset($dataSecBuyerWithoutEloq))

	    		<h4>Second Highest - Without Eloquent</h4>

	    		<table class="table table-bordered">

	    			<thead>
	    				<tr>
	    				 	<th>Buyer id</th>
	    				 	<th>Buyer Name</th>
	    				 	<th>Total Diary Taken</th>
	    				 	<th>Total Pen Taken</th>
	    				 	<th>Total Eraser Taken</th>
	    				 	<th>Total items Taken</th>
	    				</tr>
	    			</thead>
	    			<tbody>


	    				<tr>
	    				 	<td>{{$dataSecBuyerWithoutEloq['id']}}</td>
			    			<td>{{$dataSecBuyerWithoutEloq['name']}}</td>
			    			<td>{{$dataSecBuyerWithoutEloq['total_diary']}}</td>
			    			<td>{{$dataSecBuyerWithoutEloq['total_pen']}}</td>
			    			<td>{{$dataSecBuyerWithoutEloq['total_eraser']}}</td>
			    			<td>{{$dataSecBuyerWithoutEloq['total_items']}}</td>
	    				</tr>

	    			</tbody>

	    		</table>

	    	@endif

    	@else


    		@if(isset($data))

	    		<h4>Purchase List - With Eloquent</h4>

	    		<table class="table table-bordered">

	    			<thead>
	    				<tr>
	    				 	<th>Buyer id</th>
	    				 	<th>Buyer Name</th>
	    				 	<th>Total Diary Taken</th>
	    				 	<th>Total Pen Taken</th>
	    				 	<th>Total Eraser Taken</th>
	    				 	<th>Total items Taken</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    				@foreach($data as $key=>$val)

	    				<tr>
	    				 	<td>{{$val['id']}}</td>
	    				 	<td>{{$val['name']}}</td>
	    				 	<td>{{$val['total_diary']}}</td>
	    				 	<td>{{$val['total_pen']}}</td>
	    				 	<td>{{$val['total_eraser']}}</td>
	    				 	<td>{{$val['total_items']}}</td>
	    				</tr>

	    				@endforeach
	    			</tbody>

	    		</table>

    		@endif

    		@if(isset($datawithouteloq))

				<h4>Purchase List - Without Eloquent</h4>

		    	<table class="table table-bordered">

		    			<thead>
		    				<tr>
		    				 	<th>Buyer id</th>
		    				 	<th>Buyer Name</th>
		    				 	<th>Total Diary Taken</th>
		    				 	<th>Total Pen Taken</th>
		    				 	<th>Total Eraser Taken</th>
		    				 	<th>Total items Taken</th>
		    				</tr>
		    			</thead>
		    			<tbody>
		    				@foreach($datawithouteloq as $key=>$val)

		    				<tr>
		    				 	<td>{{$val['id']}}</td>
		    				 	<td>{{$val['name']}}</td>
		    				 	<td>{{$val['total_diary']}}</td>
		    				 	<td>{{$val['total_pen']}}</td>
		    				 	<td>{{$val['total_eraser']}}</td>
		    				 	<td>{{$val['total_items']}}</td>
		    				</tr>

		    				@endforeach
		    			</tbody>

		    	</table>
		    @endif
    	@endif

    </div>

</body>
</html>
